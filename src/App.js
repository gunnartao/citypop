import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link
} from "react-router-dom";
import { useState } from "react";

import HomePage from "./pages/HomePage";
import CitySearchPage from "./pages/CitySearchPage";
import CountrySearchPage from "./pages/CountrySearchPage";
import CountryCityListPage from "./pages/CountryCityListPage";
import CityInfoPage from "./pages/CityInfoPage";

export default function App() {

    const [cityData, setCityData] = useState(null); // lifted state to pass between child pages

    return (
        <Router>
            <div className="wrapper">
                <Link className="Link" to="/">CityPop</Link>
                <Switch>
                    <Route exact path="/">
                        <HomePage setCityData={setCityData} />
                    </Route>
                    <Route path="/city/search">
                        <CitySearchPage setCityData={setCityData} />
                    </Route>
                    <Route path="/country/search">
                        <CountrySearchPage setCityData={setCityData} />
                    </Route>
                    <Route path="/country/city-list">
                        <CountryCityListPage cityData={cityData} setCityData={setCityData} />
                    </Route>
                    <Route path="/city/info">
                        <CityInfoPage cityData={cityData} />
                    </Route>
                </Switch>
            </div>
        </Router>
    );
}