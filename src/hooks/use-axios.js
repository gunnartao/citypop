import { useState, useReducer, useEffect } from "react";
import axios from "axios";

export default function useAxios() {
    const [URL, setURL] = useState(null);

    const [state, dispatch] = useReducer(fetchReducer, {
        isLoading: false,
        isError: false,
        isSuccess: false,
        data: null,
    });

    // React only re-renders if the the newly passed state is different than the stored one. This is a work-around used to 
    // fetch data even if the url has not been changed, by using an object instead of a string as state (the object reference changes).
    function fetch(url) {
        setURL({url: url})
    }

    useEffect(() => {
        if (URL == null) { // do not fetch when first loading component
            return;
        }

        let didCancel = false;

        async function fetchData() {
            dispatch({ type: "INIT" });
            try {
                const result = await axios(URL.url);
                if (!didCancel) {
                    dispatch({ type: "SUCCESS", payload: result.data });
                }
            } catch (error) {
                if (!didCancel) {
                    dispatch({ type: "ERROR" });
                }
            }
        };

        fetchData();

        return () => {
            didCancel = true; // do not update if component has been unmounted
        };
    }, [URL]);

    return [state, fetch];
};

function fetchReducer(state, action) {
    switch (action.type) {
        case "INIT":
            return {
                ...state,
                isLoading: true,
                isError: false,
                isSuccess: false,
                data: null
            };
        case "SUCCESS":
            return {
                ...state,
                isLoading: false,
                isError: false,
                isSuccess: true,
                data: action.payload
            };
        case "ERROR":
            return {
                ...state,
                isLoading: false,
                isError: true,
                isSuccess: false,
                data: null
            };
        default:
            throw new Error();
    }
};