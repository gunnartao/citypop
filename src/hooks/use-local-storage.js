import { useState, useEffect } from "react";

export default function useStateWithLocalStorage(storageKey) {
    const [value, setValue] = useState(localStorage.getItem(storageKey) || "");
   
    useEffect(() => {
      localStorage.setItem(storageKey, value);
    }, [value, storageKey]);
   
    return [value, setValue];
  };