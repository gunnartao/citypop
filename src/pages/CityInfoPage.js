import { useEffect } from "react";

import useStateWithLocalStorage from "../hooks/use-local-storage";

export default function CityInfoPage({ cityData: newCityData }) {

    const [savedCityData, saveCityData] = useStateWithLocalStorage("savedCityData"); // allows refreshing of page

    useEffect(() => {
        if (newCityData) saveCityData(JSON.stringify(newCityData));
    }, [newCityData, saveCityData]);

    const cityData = newCityData || JSON.parse(savedCityData);

    try { // if we reach this page through an unexpected route (for example pasting the url), the cityData may be corrupted
        return (
            <div className="page-content">
                <h1>{cityData.cities[cityData.index].name.toUpperCase()}</h1>
                <div className="population-info">
                    <h2>
                        POPULATION
                    </h2>
                    <p>
                        {cityData.cities[cityData.index].population.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ")}
                    </p>
                </div>
            </div>
        );
    } catch {
        return <h1 className="error">Something went wrong</h1>
    }
}