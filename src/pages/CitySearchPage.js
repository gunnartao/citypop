import { useState, useEffect } from "react";
import { Redirect } from "react-router-dom";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faSearch } from "@fortawesome/free-solid-svg-icons";

import useAxios from "../hooks/use-axios";

export default function CitySearchPage({ setCityData }) {

    const [query, setQuery] = useState("");
    const [{ isLoading, isError, isSuccess, data }, fetch] = useAxios();

    function handleSubmit(event) {
        event.preventDefault();
        const formattedQuery = encodeURI(query.trim());
        fetch(`http://api.geonames.org/searchJSON?style=LONG&orderby=population&lang=en&maxRows=1&featureClass=P&username=weknowit&name=${formattedQuery}`);
    }

    function handleChange(event) {
        setQuery(event.target.value);
    }

    useEffect(() => {
        if (isSuccess && data.totalResultsCount > 0) {
            setCityData({ cities: data.geonames, index: 0 }); // pass data to city info page
        }
    }, [setCityData, data, isSuccess]);

    if (isSuccess && data.totalResultsCount > 0) { 
        return <Redirect push to="/city/info" /> // if the fetch is successfull we go to the city info page
    }

    return (
        <div className="page-content">
            <h1>SEARCH BY CITY</h1>
            <form onSubmit={handleSubmit}>
                <input type="text" placeholder="Enter a city" onChange={handleChange}></input>
                <br></br>
                <button className="search-submit" type="submit">
                    <FontAwesomeIcon icon={faSearch} size="3x" />
                </button>
            </form>
            {
                (isLoading && <span className="status">LOADING...</span>) ||
                (isSuccess && <span className="status error">NO MATCHES FOUND</span>) || // if any matches are found we are redirected earlier
                (isError && <span className="status error">FAILURE FETCHING DATA</span>)
            }
        </div>
    );
}