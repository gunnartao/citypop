import { useEffect } from "react";
import { useHistory } from "react-router-dom";

import useStateWithLocalStorage from "../hooks/use-local-storage";

export default function CountryCityListPage({ cityData: newCityData, setCityData: setNewCityData }) {

    const history = useHistory();
    const [savedCityData, saveCityData] = useStateWithLocalStorage("savedCityData"); // allows refreshing of page

    useEffect(() => {
        if (newCityData) saveCityData(JSON.stringify(newCityData));
    }, [newCityData, saveCityData]);

    const cityData = newCityData || JSON.parse(savedCityData);

    function handleClick(event) {
        setNewCityData({ ...cityData, index: event.target.id });
        history.push("/city/info");
    }

    try { // if we reach this page through an unexpected route (for example pasting the url), the cityData may be corrupted
        return (
            <div className="page-content">
                <h1>{cityData.cities[0].countryName.toUpperCase()}</h1>
                <ol>
                    {cityData.cities.map((city, index) => (
                        <li key={city.geonameId}>
                            <button className="city-item" id={index} onClick={handleClick}>{city.name.toUpperCase()}</button>
                        </li>
                    ))}
                </ol>
            </div>
        )
    } catch {
        return <h1 className="error">Something went wrong</h1>
    }
}