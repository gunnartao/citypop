import { useState, useEffect } from "react";
import { Redirect } from "react-router-dom";
import countryEncoder from "i18n-iso-countries";
import en from "i18n-iso-countries/langs/en.json";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faSearch } from "@fortawesome/free-solid-svg-icons";

import useAxios from "../hooks/use-axios";

export default function CountrySearchPage({ setCityData }) {

    const [query, setQuery] = useState("");
    const [isCountry, setIsCountry] = useState(true);
    const [{ isLoading, isError, isSuccess, data }, fetch] = useAxios();

    function handleSubmit(event) {
        event.preventDefault();
        countryEncoder.registerLocale(en);
        const countryCode = countryEncoder.getAlpha2Code(query.trim(), "en"); // the geonames api needs country codes
        if (countryCode) {
            setIsCountry(true);
            fetch(`http://api.geonames.org/searchJSON?style=LONG&orderby=population&maxRows=3&lang=en&featureClass=P&username=weknowit&country=${countryCode}`);
        } else {
            setIsCountry(false);
        }
    }

    function handleChange(event) {
        setQuery(event.target.value);
    }

    useEffect(() => {
        if (isSuccess && data.totalResultsCount > 0) {
            setCityData({ cities: data.geonames }); // pass data to country city-list page
        }
    }, [setCityData, data, isSuccess]);

    if (isSuccess && data.totalResultsCount > 0) {
        return <Redirect push to="/country/city-list" /> // if the fetch is successfull we go to the country city-list page
    }

    return (
        <div className="page-content">
            <h1>SEARCH BY COUNTRY</h1>
            <form onSubmit={handleSubmit}>
                <input type="text" placeholder="Enter a country" onChange={handleChange}></input>
                <br></br>
                <button className="search-submit" type="submit">
                    <FontAwesomeIcon icon={faSearch} size="3x" />
                </button>
            </form>
            { // The order of these conditions should not be changed without care
                (isLoading && <span className="status">LOADING...</span>) ||
                ((!isCountry || isSuccess) && <span className="status error">NO MATCHES FOUND</span>) ||
                (isError && <span className="status error">FAILURE FETCHING DATA</span>)
            }
        </div>
    );
}