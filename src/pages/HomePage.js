import { useHistory } from "react-router-dom";

export default function HomePage() {

    const history = useHistory();

    function handleCitySearchClick() {
        history.push("/city/search");
    }

    function handleCountrySearchClick() {
        history.push("/country/search");
    }

    return (
        <div className="page-content">
            <div className="select-search">
                <button onClick={handleCitySearchClick}>
                    SEARCH BY CITY
                </button>
                <button onClick={handleCountrySearchClick}>
                    SEARCH BY COUNTRY
                </button>
            </div>
        </div>
    );
}